package com.example.rothnaksomang.mvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rothnaksomang.mvp.presenter.IPresenter;
import com.example.rothnaksomang.mvp.presenter.LoginPresenter;
import com.example.rothnaksomang.mvp.view.ILoginView;

public class MainActivity extends AppCompatActivity implements ILoginView{

    EditText etUsername,etPassword;
    Button btnLogin;

    IPresenter iPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin=findViewById(R.id.btnLogin);
        etUsername=findViewById(R.id.etUsername);
        etPassword=findViewById(R.id.etPassword);

        iPresenter=new LoginPresenter((ILoginView) this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iPresenter.onLogin(etUsername.getText().toString(),etPassword.getText().toString());

            }
        });


    }

    @Override
    public void onLoginResult(String message) {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }
}
