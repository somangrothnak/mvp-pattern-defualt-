package com.example.rothnaksomang.mvp.presenter;

import com.example.rothnaksomang.mvp.model.User;
import com.example.rothnaksomang.mvp.view.ILoginView;

public class LoginPresenter implements IPresenter {

    ILoginView iLoginView;

    public LoginPresenter(ILoginView iLoginView){
        this.iLoginView=iLoginView;
    }

    @Override
    public void onLogin(String username, String password) {
        User user =new User(username,password);
        Boolean isLoginSuccess=user.isValidData();
        if(isLoginSuccess){
            iLoginView.onLoginResult("Login Successful");
        }else{
            iLoginView.onLoginResult("Login Not Successful");
        }
    }
}
