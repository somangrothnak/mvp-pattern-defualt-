package com.example.rothnaksomang.mvp.model;

import android.text.TextUtils;
import android.util.Patterns;

import java.util.regex.Pattern;

public class User implements IUser{
    private String username;
    private String passwrod;

    public User(){}

    public User(String username, String passwrod) {
        this.username = username;
        this.passwrod = passwrod;
    }


    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return passwrod;
    }

    @Override
    public Boolean isValidData() {
        return !TextUtils.isEmpty(username) &&
                Patterns.EMAIL_ADDRESS.matcher(username).matches() &&
                passwrod.length()>6;
    }
}
