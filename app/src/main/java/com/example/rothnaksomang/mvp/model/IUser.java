package com.example.rothnaksomang.mvp.model;

public interface IUser {
    String getUsername();
    String getPassword();
    Boolean isValidData();
}
